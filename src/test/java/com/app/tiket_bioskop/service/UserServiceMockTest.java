package com.app.tiket_bioskop.service;

import com.app.tiket_bioskop.entity.user.Users;
import com.app.tiket_bioskop.repository.UserRepository;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

class UserServiceMockTest {

    @Mock
    private UserRepository userRepository;

    private UserService userService;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.openMocks(this);
        this.userService = new UserServiceImpl(userRepository);
    }

    @Test
    void updateUser() {
        Users updatedUser = new Users();
        Assertions.assertDoesNotThrow(() -> userService.updateUser(updatedUser));
    }

    @Test
    void deleteUser() {
        Assertions.assertDoesNotThrow(() -> userService.deleteUser(1));
    }

    @Test
    void showAllUsers() {
        Assertions.assertEquals(userRepository.findAll(), userService.showAllUsers());
    }

    @Test
    void getUserById() {
        Assertions.assertEquals(userRepository.getUserById(1), userService.getUserById(1));
    }

}
