package com.app.tiket_bioskop.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.EmbeddedId;
import javax.persistence.Entity;
import javax.persistence.Table;

@Setter
@Getter
@Entity
@Table(name = "seats")
public class Seats {

    @EmbeddedId
    private SeatId seatId;
}
