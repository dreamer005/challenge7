package com.app.tiket_bioskop.entity.user;

public enum ERole {
    ADMIN, CUSTOMER
}
