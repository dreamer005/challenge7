package com.app.tiket_bioskop.service;

import com.app.tiket_bioskop.entity.user.Users;

import java.util.List;

public interface UserService {

    void addUser(Users users);
    void updateUser(Users users);
    void deleteUser(Integer userId);

    //FOR JASPER
    Users getUserById(Integer userId);

    List<Users> showAllUsers();
}
