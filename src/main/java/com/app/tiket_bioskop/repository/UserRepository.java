package com.app.tiket_bioskop.repository;

import com.app.tiket_bioskop.entity.user.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository // Menandakan sebagai kelas Repository
@Transactional // Untuk pengoperasian database
// pengoperasian DB, Select, Save, Update, Delete
// T : Table, ID : tipe data primary key di table
public interface UserRepository extends JpaRepository<Users, Integer> {

    Boolean existsByUsername(String username);

    Boolean existsByEmail(String email);

    // Hitung jumlah data yang ada di DB, return Integer
    @Query("select count(1) from users")
    Integer countUsers();

    // Cari by Username
    @Query(value = "select * from users where username = :username", nativeQuery = true)
    Users findByUsername(
            @Param("username") String username
    );

    // Add a user
    @Modifying
    @Query(nativeQuery = true, value = "insert into users(username, email, password) values(:username, :email, :password)")
    void addUser(
            @Param("username") String username,
            @Param("email") String email,
            @Param("password") String password
    );

    // Update data a user
    @Modifying
    @Query(nativeQuery = true, value = "update users set username= :username, email= :email, password= :password where user_id= :user_id")
    void updateUser(
            @Param("username") String username,
            @Param("email") String email,
            @Param("password") String password,
            @Param("user_id") Integer userId
    );

    // Delete a user
    @Modifying
    @Query(nativeQuery = true, value = "delete from users where user_id= :user_id")
    void deleteUser(
            @Param("user_id") Integer userId
    );

    // Show all Users
    // Pake findAll() dari JPA

    // show selected user by id
    @Query(nativeQuery = true, value = "select * from users where user_id = :user_id")
    Users getUserById(
            @Param("user_id") Integer userId
    );

}
